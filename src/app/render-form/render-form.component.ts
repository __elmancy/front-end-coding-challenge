import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-render-form',
  templateUrl: './render-form.component.html',
  styleUrls: ['./render-form.component.scss']
})
export class RenderFormComponent implements OnInit {
  formData: any = [
    {
      "type":"header",
      "subtype":"h6",
      "label":"label",
      "access":false
    },
    {
      "type":"text",
      "required":false,
      "label":"Text Field",
      "className":"form-control",
      "name":"text-1614116740174",
      "access":false,
      "subtype":"text"
    },
    {
      "type":"select",
      "required":false,
      "label":"DropDown",
      "className":"form-control",
      "name":"select-1614116708290",
      "access":false,
      "multiple":false,
      "values":[
        {
          "label":"Option 1",
          "value":"option-1",
          "selected":true
        },
        {
          "label":"Option 2",
          "value":"option-2",
          "selected":false
        },
        {
          "label":"Option 3",
          "value":"option-3",
          "selected":false
        }
      ]
    },
    {
      "type":"checkbox-group",
      "required":false,
      "label":"Checkbox Group",
      "toggle":false,
      "inline":false,
      "name":"checkbox-group-1614116755641",
      "access":false,
      "other":false,
      "values":[
        {
          "label":"Option 1",
          "value":"option-1",
          "selected":true
        }
      ]
    },
    {
      "type":"radio-group",
      "required":false,
      "label":"Radio Group",
      "inline":false,
      "name":"radio-group-1614116890535",
      "access":false,
      "other":false,
      "values":[
        {
          "label":"Option 1",
          "value":"option-1",
          "selected":true
        },
        {
          "label":"Option 2",
          "value":"option-2",
          "selected":false
        },
        {
          "label":"Option 3",
          "value":"option-3",
          "selected":false
        }
      ]
    },
    {
      "type":"button",
      "subtype":"submit",
      "label":"Submit",
      "className":"btn-primary btn",
      "name":"button-1614116764780",
      "access":false,
      "style":"primary"
    },
    {
      "type":"button",
      "label":"Cancel",
      "subtype":"button",
      "className":"btn-danger btn",
      "name":"button-1614116905393",
      "access":false,
      "style":"danger"
    }
  ];
  startRendering: boolean = false;
  constructor() { }

  ngOnInit() {
    this.formData = JSON.stringify(this.formData);
    setTimeout(() => {
      this.render();
    }, 2000);
  }

  render() {
    this.startRendering = true;
    const options = {
      dataType: 'json', formData: this.formData
    };
    $('#fb-render').formRender(options);

  }

}
