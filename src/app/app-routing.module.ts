import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GenerateFormComponent} from './generate-form/generate-form.component';
import {RenderFormComponent} from './render-form/render-form.component';


const routes: Routes = [
  {path: '', redirectTo: 'render-form', pathMatch: 'full'},
  {path: 'render-form', component: RenderFormComponent, data: {title: 'Render Form'}},
  {path: 'generate-form', component: GenerateFormComponent, data: {title: 'Generate Form'}},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
