import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-generate-form',
  templateUrl: './generate-form.component.html',
  styleUrls: ['./generate-form.component.scss']
})
export class GenerateFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    var options = {
      disabledActionButtons: ['save']
    };
    $(document.getElementById('fb-editor')).formBuilder(options);
  }

}
